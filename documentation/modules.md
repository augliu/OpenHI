# Modules
* Image pre-segmentation
* Database
  * Point filtering
  * Point recording
  * Point requesting
* Image blending
  * Annotation color generator (single level)
  * Annotation color fusing module (multiple level)
  * Annotation color to original image painting module (two matrices)
  * WSI boundary overlaying module
* Image reading
  * WSI *(read_wsi)*
  * Boundary *(read_boundary)*

# Basic values
**of the viewing system** *(viewsys_)*

| Name | Description | viewsys_ | 
| ---- | ----------- | ---- | 
| WSI width | *see image* | 
| WSI height | *see image* |
| viewing top-left coordinate | *see image* | tl | 
| viewing bottom-right coordinate | *see image* |
| viewing size | tl (minus) br | va (viewing area) |
| viewer width | *see image* | sz[0] |
| viewer height | *see image* | sa [1] | 


![Selection_038](coordinate_system.png)

# Detail about the module
## Image blending
### Annotation color generator (single level) 
function: *wsi_gen_color* <br />
This function generate an image with annotation color in it. 
The annotation points and color is specified by the inputs.

**Input**
* ***viewsys:*** viewing system state
* ***boundary_view:*** viewing boundary image
* ***anno_point:*** annotation points acquired by the database retrieval module
* ***region_color:*** (tuple) RGB tuple specifying the annotation color

**Output**
* ***return:*** RGB image with specified annotation color

### Annotation color fusing module (multiple level) 
function: *wsi_color_fuse* <br />
This is a simple function for merging many "RGB image with specified annotation color"
into one image with more than one RGB color. 

**Input**
* ***img_stack:*** (list) a list containing multiple images

**Output**
* ***return:*** fused RGB image with specified annotation color

### Annotation color to original image painting module (two matrices) 
function: *wsi_color_paint* <br />
This function fuses the single or multi- color annotation image into 
one image with **alpha** value (1 = full color and 0 = keep the original image)

**Input**
* ***wsi_view:*** viewing wsi
* ***color_view:*** viewing color annotation image

**Output**
* ***return:*** fused RGB image with specified annotation color and alpha value

### WSI boundary overlaying module
function: *wsi_mark_boundary* <br />
This function overlays the boundary image on the given input image
(it can be colored by the annotation or just an original image). 
Currently, we only support 100% black edges.

**Input**
* ***bg:*** (RGB array)background image
* ***boundary_view:*** (binary array)viewing boundary image

**Output**
* ***return:*** fused RGB image with boundary markings


## Image reading
### Read part of the WSI 
function: *read_wsi* <br />
**Input**
* ***viewsys:*** viewing system state

**Output**
* ***return:*** Image array

### Read part of the WSI boundary
function: *read_boundary* <br />
This function is a processing/viewing part from the big binary 
boundary image and resize it to specified size. 
**Input**
* ***viewsys:*** viewing system state

**Output**
* ***return:*** Binary image array

