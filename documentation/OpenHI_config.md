# OpenHI configuration file

***Location:*** <br />
*static/OpenHI_conf_example.ini* **OR** <br />
*static/OpenHI_conf.ini*

The file *static/OpenHI_conf_example.ini* is provided in the source code. However,
the configuration may differ from machine to machine. Therefore we provide local and
external configuration file which is *static/OpenHI_conf.ini*. If local configuration 
is to be used, one should copy the example file and make their own copy of the 
configuration file according to the example file. 

## Database module 
**Name:** db
* **_host:_** MySQL database address
* **_port:_** MySQL database port (default = 3306)
* **_user:_** Username of the MySQL database
* **_passed:_** Password of the MySQL database
* database: MySQL schema name

## Framework
**Name:** framework
* **_host:_** Intended serving address. Use 127.0.0.1 for locally testing on the machine
    and 0.0.0.0 for local network test.
* **_port:_** Testing port. Flask's default is *5000*, HTTP default is *80*. 
* **_debug:_** Enable or disable Flask's debug mode with value of 'True' or 'False'.
* **_reloader:_** Enable or disable Flask's reloader with value of 'True' or 'False'.

## Annotation project configuration
**Name:** project
* **_pslv:_** maximum presegmentation level (start from 1)
* **_pslv_val:_** sub-region average value of each pslv

Note: _pslv = pre-segmentation level_

## Annotator viewer initialization configuration
**Name:** viewer_init
* **_slide_id:_** maximum presegmentation level (start from 1)
* **_viewer_coor:_** sub-region average value of each pslv

## SSH configuration used to download metadata from remote server
**Name:** viewer_init
* **_hostname:_** (Remote server address)
* **_port:_** (The port used to download data)
* **_username:_** (The username used to connect remote server)
* **_password:_** (Password of the user)

## Configuration related to virtual magnification
**Name:** virtual_mag
* **_ignore_human_pixel_size:_** *(True/False)* Select on or off to ignore human pixel size calculation factor

## Configuration related to performing annotation
**Name:** annotation
* **_manifest_filename:_** _(string)_ The manifest filename in /framework_src/ directory that you want to use. 
(See manifest example file for formatting)
* **_max_slide_number:_** _(int)_ The maximum number of slide allowed to be changed. 
* **_max_grading:_** _(int)_ The maximum number of grades that is allowed for annotation. 
* **_pslv_val:_** _(Python:list)_ The list of available pre-processed PSLV. 
* **_grading_specific_check:_** Enable or disable checking whether the selected region has been labeled only in the same grade , with value of 'True' or 'False'.
* **_diff_pslv_check:_** Enable or disable checking whether the selected region has been labeled only in the same PS level, with value of 'True' or 'False'.