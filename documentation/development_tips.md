# OpenHI development tips and workflow

If anyone wants to develop on OpenHI, the following development workflow
that we use may help to make the development goes easier. 

* We based the OpenHI on Flask web framework. 
    * The template folder contains all the HTML files used by Flask. 
    * The main script is ***app.py***
    * In addition to Flask file scheme, we add supporting python-script
    in the root directory of the project. Files starting with "anno_*" 
    is a part of OpenHI framework. 
* For image pre-processing module, there are two versions
    * ***MATLAB*** version can be run directly in *MATLAB*. The script
    in `img_preproc_matlab` is self-contained.
    * ***opensource*** version is developed after the *MATLAB* version
    and is supposed to work in an equivalent way. However, this module
    is highly experimental. The code in opensource image pre-processing
    is also self-containd at `img_preproc_opensource`.
