"""This is TCGA WSI connector (fullpath caller). This script is not meant to be run as a main function."""
import json
import os

# Load configuration JSON file
with open('/home1/pp/docs/py-proj/flask-anno2/img_preproc_opensource_opensource/tcga_connector_conf.json') as json_data_file:
    conf = json.load(json_data_file)


# Function to provide fullpath to the WSI.
def get_fullpath(slide_number):

    return '20x.png'


if __name__ == '__main__':
    print('This script is not meant to be run as a main function. Please refer to the documentation')


