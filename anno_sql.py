# This script contains custom-made function for the WSI Annotator
#   Created on 20180703

import os
import configparser
import mysql.connector
import anno_img as img
import numpy as np
import re


class CurrentStaticObject:
    def __init__(self):
        self.tcga_uuid = '1234567890'
        self.annotator_id = 1
        self.slide_id = 1


class PointDynamicObject:
    def __init__(self, x=[], y=[], grading=None,
                 region=[], selected=[], annobatch=None):
        self.x = x
        self.y = y
        self.grading = grading
        self.pslv = None
        self.region = region
        self.selected = selected
        self.annobatch = annobatch

    def update_grading(self, new_grading):
        self.grading = str(int(new_grading))

    def update_pslv(self, new_pslv):
        self.pslv = str(int(new_pslv)+1)
        print(self.pslv)

    def update_annobatch(self, new_batch):
        self.annobatch = new_batch

    def update_region(self, i, new_region):
        self.region[i] = new_region

    def update_selected(self, i, new_selected):
        if new_selected:
            self.selected[i] = 1
        else:
            self.selected[i] = 0

    def add_point_dump(self, x, y):
        self.x.append(x)
        self.y.append(y)
        self.region.append(0)
        self.selected.append(2)

    def clear_point_dump(self):
        self.x = []
        self.y = []
        self.selected = []
        self.region = []
        print('xy dump has been cleared')

    def get_coor(self):
        coor = []
        for i in range(len(self.x)):
            coor.append([self.x[i], self.y[i]])
        return coor


def create_connector():
    """
    Connect to MySQL server and database based on preset values. Return database object.
    Input: This function require the OpenHI configuration file. Example conf can be found at "static/OpenHI_conf.ini".
    :return: MySQL database object
    """
    # Check if the local configuration file exists, if not run based on the example file.
    file_checking = 'static/OpenHI_conf.ini'
    checking_status = os.path.isfile(file_checking)

    if not checking_status:
        open_file_name = 'static/OpenHI_conf_example.ini'
    else:
        open_file_name = file_checking

    # Read the OpenHI INI configuration file.
    conf = configparser.ConfigParser()
    conf.read(open_file_name)

    # Read necessary information from dictionary object
    conf_host = conf['db']['host']
    conf_port = conf['db']['port']
    conf_user = conf['db']['user']
    conf_passwd = conf['db']['passwd']
    conf_database = conf['db']['database']

    # Create database object based on the given configuration
    mydatabase = mysql.connector.connect(
        host=conf_host,
        port=conf_port,
        user=conf_user,
        passwd=conf_passwd,
        database=conf_database
    )

    return mydatabase


def re_create_connector(mydatabase):
    """
    If the connection is timeout,then reconnect to MySQL server and database based on preset values.If not,keep the old connection.Return database object.
    :return: MySQL database object
    """
    try:
        mydatabase.cursor()

    except Exception:
        mydatabase.close()

        file_checking = 'static/OpenHI_conf.ini'
        checking_status = os.path.isfile(file_checking)

        if not checking_status:
            open_file_name = 'static/OpenHI_conf_example.ini'
        else:
            open_file_name = file_checking

        # Read the OpenHI INI configuration file.
        conf = configparser.ConfigParser()
        conf.read(open_file_name)

        # Read necessary information from dictionary object
        conf_host = conf['db']['host']
        conf_port = conf['db']['port']
        conf_user = conf['db']['user']
        conf_passwd = conf['db']['passwd']
        conf_database = conf['db']['database']

        # Create database object based on the given configuration
        mydatabase = mysql.connector.connect(
            host=conf_host,
            port=conf_port,
            user=conf_user,
            passwd=conf_passwd,
            database=conf_database
        )
    return mydatabase


'''
def record_point1(db, so, do):
    rec_time = '2018-07-03 23:28:26'
    x = int(float(do.x))
    y = int(float(do.y))
    sql = 'insert into point (x, y, annotation_ts, grading_id, pslv_id, slide_id, annotator_id) VALUES (%s, %s, %s, %s, %s, %s, %s)'
    # val = (1, 1, 'now()', 1, 1, 1, 1)
    val = (x, y, rec_time, do.grading, do.pslv, so.slide_id, so.annotator_id)
    # print('Printing val: ')
    # print(val)

    mycursor = db.cursor()
    mycursor.execute(sql, val)
    db.commit()
'''


def set_point(anno_id, slide_id, dobj, loader, db):
    print('setting points...')
    bwboun_img = loader.imgCurrentBWBounView
    tl_coor = loader.imgTLCorr
    print('loader.maxRegionID: ' + str(loader.maxRegionID))
    num_region = loader.maxRegionID
    anno_batch = get_batch_for_record(db, anno_id, slide_id) + 1
    # dobj.update_annobatch(anno_batch)

    #rec_time = '2018-07-03 23:28:26'
    num_of_entries = len(dobj.x)

    loop_val = []
    boundary = 0
    for i in range(num_of_entries):
        # Construct the val

        if dobj.region[i] == 0:

            seedpoint = (int(float(dobj.x[i])), int(float(dobj.y[i])))
            # print(str(i) + ': ' + str(seedpoint))

            coor_list = img.get_assoc_coor(bwboun_img, tl_coor, seedpoint)
            # print('len_coor_list: %s'%len(coor_list))

            if len(coor_list) == 0:
                boundary = boundary + 1
                continue
            else:
                # print(i)
                # print('first is 0')
                num_region = num_region + 1

                dobj.region[i] = num_region
                dobj.selected[i] = 1
                # dobj.update_region(i, num_region)
                # dobj.update_selected(i, True)

                # for-loop: for cheching the rest points in one-drag
                x_max = np.max(coor_list[:,0])
                y_max = np.max(coor_list[:,1])
                x_min = np.min(coor_list[:,0])
                y_min = np.min(coor_list[:,1])
                for j in range(i + 1, num_of_entries):
                    if(int(float(dobj.x[j]))<=x_max and int(float(dobj.x[j]))>=x_min and int(float(dobj.y[j]))<=y_max and int(float(dobj.y[j]))>=y_min):
                    #if (int(float(dobj.x[j])), int(float(dobj.y[j]))) in coor_list :
                        for checktest in coor_list:
                            if int(float(dobj.x[j]))==checktest[0] and int(float(dobj.y[j]))==checktest[1] :
                                dobj.region[j] = dobj.region[i]
                                dobj.selected[j] = 0
                                break

                    #checkpoint = [int(float(dobj.x[j])), int(float(dobj.y[j]))]
                    ## print(checkpoint)
                    #if checkpoint not in coor_list: #  it is totally wrrong!!!!!
                    #    dobj.region[j] =0
                    #else:
                    #    dobj.region[j] = dobj.region[i]
                    #    dobj.selected[j] = 0
                    #    print(checkpoint)
                        # dobj.update_region(j, dobj.region[i])
                        # dobj.update_selected(j, False)

        loop_val.append((dobj.x[i], dobj.y[i], dobj.grading, dobj.pslv, slide_id,
                         anno_id, dobj.region[i], dobj.selected[i], anno_batch))

    print('%s points on boundary' % boundary)

    loader.update_maxRegionID(num_region)
    loader.update_maxAnnobatch(anno_batch)

    return loop_val


def check_record_point(db, tl_coor, viewer_size, sobj, dobj, loader, asess):
    # Insert the recording point validation code in this section ###

    checking_img = asess.get_current_annotation(sobj.annotator_id)
    bwboun_img = img.img_crop(loader.imgBWBoun[loader.current_pslv], tl_coor, viewer_size, 0)
    # seedpoints = [[1800, 1800], [2200, 2200]]
    for i in range (len(dobj.x)):
        seedpoints = [[int(float(dobj.x[i])),int(float(dobj.y[i]))]]
        returning_result = img.check_for_overlapping_regions(checking_img, bwboun_img, tl_coor, seedpoints, 1)
        print('The checking result is: ')
        print(returning_result[0,0])
        if returning_result[0,0]:
            dobj.success.append(dobj[i])
        else:
            dobj.false.append(dobj[i])
    return dobj
    # --------------------------[end]--------------------------- ###


def record_point2(db, tl_coor, viewer_size, annotator_id, slide_id, dobj, loader, asess):
    """
    This function properly records annotation points from the annotator.
    :param db: database object
    :param tl_coor: (1x2 tuple) top-left coordinate
    :param viewer_size: (1x2 tuple) the viewing size
    :param sobj: static object of OpenHI
    :param dobj: dynamic object of OpenHI
    :param loader: LoaderConfiguration of OpenHI
    :param asess: annotator object of OpenHI
    :return: number of recorded points that have been successfully recorded to MySQL database.
    """


    print('-----------------call record-point2 funtion----------------')
    loop_val = set_point(annotator_id, slide_id, dobj, loader, db)

    sql = 'insert into point (x, y, annotation_ts, grading_id, pslv_id, slide_id, annotator_id, region_id, selected, anno_batch) ' \
          'VALUES (%s, %s, now(), %s, %s, %s, %s, %s, %s, %s)'
    mycursor = db.cursor()
    mycursor.executemany(sql, loop_val)
    db.commit()
    mycursor.close()
    num_record = len(loop_val)
    return num_record


def get_numRegion(db, slide_id ):
    sql = 'SELECT max(region_id) FROM point WHERE slide_id = ' + str(slide_id)  # db_wsi1_dev.
    mycursor = db.cursor()
    mycursor.execute(sql)
    tmp = mycursor.fetchone()[0]
    mycursor.close()

    if tmp is None:
        maxRegionID = 0
    else:
        maxRegionID = tmp

    return maxRegionID


def get_batch(db, pslv, annotator_id, slide_id):
    """
    This function can fetch current maximum annotation batch from the database according to given pslv, annotator, and
    slide ID.
    :param db: OpenHI MySQL database connector object
    :param pslv: specific pslv that max annotation batch number is needed
    :param annotator_id: annotator id
    :param slide_id: slide id
    :return: (int) max annotation batch from MySQL database
    """
    sql = 'SELECT max(anno_batch) FROM point WHERE annotator_id = ' + str(annotator_id) \
          + ' AND slide_id = ' + str(slide_id) + ' AND pslv_id = ' + str(pslv)

    mycursor = db.cursor()
    mycursor.execute(sql)
    tmp = mycursor.fetchone()[0]
    mycursor.close()

    if tmp is None:
        anno_batch = 0
        print('no points.')
    else:
        anno_batch = tmp

    print('Current annotation batch is: ', str(anno_batch))

    return anno_batch


def get_batch_for_record(db, annotator_id, slide_id):
    sql = 'SELECT max(anno_batch) FROM point WHERE annotator_id = ' + str(annotator_id) \
          + ' AND slide_id = ' + str(slide_id)
    mycursor = db.cursor()
    mycursor.execute(sql)
    tmp = mycursor.fetchone()[0]
    mycursor.close()

    if tmp is None:
        anno_batch = 0
    else:
        anno_batch = tmp
    return anno_batch


# deletion
def delete_point(db, dobj, loader, annotator_id, slide_id):
    """db = database object, tl = top-left coordinate, sz = size of (*changed)viewing area """

    num_delete = 0
    loop_val = set_point(annotator_id, slide_id, dobj, loader, db)
    for i in range(len(loop_val)):
        if loop_val[i][7] == 1:
            # deletion
            xy_coor = [loop_val[i][0], loop_val[i][1]]
            pslv = dobj.pslv
            r_id = search_region(db, xy_coor, pslv, loader, annotator_id, slide_id)
            while r_id != 0 and r_id != None:
                # do deletion
                delete_region(db, r_id, pslv, annotator_id, slide_id)
                num_delete = num_delete + 1
                print('Region %s ---Deletion Finished.' % r_id)
                r_id = search_region(db, xy_coor, pslv, loader, annotator_id, slide_id)
            # else:
            #     print('Deletion Error.')
    print ('has deleted %s region' % num_delete)
    return num_delete


def search_region(db, xy_coor, pslv, loader, annotator_id, slide_id):
    """db = database object, tl = top-left coordinate, sz = size of (*changed)viewing area """

    bwboun_img = loader.imgCurrentBWBounView
    tl = loader.imgTLCorr

    # should only have one point in dobj
    seedpoint = (int(float(xy_coor[0])), int(float(xy_coor[1])))
    # print(str(i) + ': ' + str(seedpoint))
    coor_list = img.get_assoc_coor(bwboun_img, tl, seedpoint)
    #a= np.array(coor_list)
    if len(coor_list) == 0:
        return 0
    x_max = np.amax(coor_list[:,0])
    y_max = np.amax(coor_list[:,1])
    x_min = np.amin(coor_list[:,0])
    y_min = np.amin(coor_list[:,1])

    sql_cmd = 'SELECT * FROM point WHERE x >' + str(x_min) + ' AND x <' + str(x_max) \
              + ' AND y > ' + str(y_min) + ' AND y < ' + str(y_max) \
              + ' AND selected = 1 AND annotator_id = ' + str(annotator_id) + ' AND slide_id = ' + str(slide_id) \
              + ' AND pslv_id = ' + str(pslv)
    print(sql_cmd)

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    myresult = mycursor.fetchall()
    mycursor.close()

    checkpoint_list = sql_result_exchange(myresult)
    check = False
    for dobj in checkpoint_list:
        checkpoint= dobj.get_coor()[0]
        for checktest in coor_list:
            if int(float(checkpoint[0]))==checktest[0] and int(float(checkpoint[1]))==checktest[1] :
                if dobj.region[0] != 0:
                    check = True
                    r_id = dobj.region[0]
                    return r_id
                    break
    return 0


def delete_region(db, r_id, pslv, annotator_id, slide_id):
    sql_cmd = 'UPDATE point' + ' SET region_id = 0' +' WHERE region_id = ' + str(r_id) + \
              ' AND annotator_id = ' + str(annotator_id) + ' AND slide_id = ' + str(slide_id) \
              + ' AND pslv_id = ' + str(pslv)
    print(sql_cmd)

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    db.commit()
    mycursor.close()


# undo
def undo_batch(db, undo_num, annotator_id, slide_id, loader):

    maxAnnobatch=loader.maxAnnobatch
    # maxAnnobatch = get_batch_for_record(db, sobj)
    # maxAnnobatch = get_batch(db, pslv, sobj)
    # print(maxAnnobatch)

    if maxAnnobatch == 0:
        return

    if undo_num > maxAnnobatch:
        print('can not undo %s times.' % undo_num)
        return 'retry'
    else:
        sql_cmd = 'UPDATE point' + ' SET region_id = 0 WHERE anno_batch > ' + str(maxAnnobatch - undo_num) \
                  + ' AND annotator_id = ' + str(annotator_id) + ' AND slide_id = ' + str(slide_id)
        print(sql_cmd)
        mycursor = db.cursor()
        mycursor.execute(sql_cmd)
        db.commit()
        mycursor.close()
        loader.update_maxAnnobatch(maxAnnobatch - undo_num)
        return 'undo successfully.'


# change the result for a sql_msg to list of PointDynamicObject
def sql_result_exchange(point_list):
    # point_list is the result for a SELECT * FROM ...
    dobj_list = []
    for p in point_list:
        (x, y) = ([p[1]], [p[2]])
        grading = [p[4]]
        region = [p[8]]
        selected = [p[9]]
        annobatch = p[10]

        # each dobj should only have one point, that is len(x) = 1
        dobj = PointDynamicObject(x, y, grading, region, selected, annobatch)
        dobj_list.append(dobj)

    return dobj_list


# Get a list of annotated points according to pre-segmentation level
def get_record(db, tl, sz, max_pslv, annotator_id, slide_id):
    """db = database object, tl = top-left coordinate, sz = size of (*changed)viewing area """
    annotated_points = list()

    for loop_lv in range(max_pslv):
        x_min = tl[0]
        x_max = tl[0] + sz[0]
        y_min = tl[1]
        y_max = tl[1] + sz[1]

        x_min = str(x_min)
        x_max = str(x_max)
        y_min = str(y_min)
        y_max = str(y_max)

        sql_cmd = (
                'SELECT x, y, grading_id '+
                'FROM point WHERE '+
                'x > ' + x_min + ' AND x < ' + x_max + ' AND y > ' + y_min + ' AND y < ' + y_max
                + ' AND selected = 1'
                + ' AND pslv_id = ' + str(loop_lv + 1)
                + ' AND slide_id = ' + str(slide_id)
                + ' AND annotator_id = ' + str(annotator_id)
                + ' AND region_id <> 0 '
        )
        print('MySQL Command: ' + sql_cmd)

        cursor = db.cursor()
        cursor.execute(sql_cmd)
        read_result = cursor.fetchall()

        cursor.close()
        print('Length of the result: ' + str(len(read_result)))

        annotated_points.append(read_result)

    return annotated_points


def get_patient_data(property,slide_id,db):

    meta_dict = {}

    sql1 = "SELECT tcga_case_id from wsi WHERE slide_id=" + slide_id
    mycursor = db.cursor(buffered=True)
    mycursor.execute(sql1)
    tcga_case_id = str(mycursor.fetchone()[0])
    mycursor.close()

    if property == 'demographic':

        sql = 'SELECT JSON_EXTRACT(patient_info,\'$**."clin_shared:ethnicity".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."shared:gender".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."clin_shared:race".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."clin_shared:days_to_birth".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."clin_shared:days_to_death".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."clin_shared:vital_status".text\') from patient where tcga_case_id=\''+tcga_case_id+'\';'

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

        meta_dict.setdefault('ethnicity',tmp[0])
        meta_dict.setdefault('gender', tmp[1])
        meta_dict.setdefault('race', tmp[2])
        try:
            meta_dict.setdefault('days_to_birth', re.search(r'(\[\")(-)(.*)(\"\])', tmp[3]).group(3)+" days")
        except:
            if tmp[3] is None:
                meta_dict.setdefault('days_to_birth', 'Unknown')
        try:
            meta_dict.setdefault('days_to_death', re.search(r'(\[\")(.*)(", ")(.*)(\"\])',tmp[4]).group(2)+" days")
        except:
            if tmp[4] is None:
                meta_dict.setdefault('days_to_death', 'Unknown')
        meta_dict.setdefault('vital_status', tmp[5])

    if property == 'diagnosis':

        sql = 'SELECT JSON_EXTRACT(patient_info,\'$**."days_to_last_followup".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."shared:neoplasm_histologic_grade".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."shared_stage:clinical_stage".text\'), ' \
              'JSON_EXTRACT(patient_info,\'$**."shared_stage:clinical_stage".text\') from patient where tcga_case_id=\''+tcga_case_id+'\''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

        meta_dict.setdefault('days_to_last_followup', tmp[0])
        meta_dict.setdefault('tumor_grade', tmp[1])
        meta_dict.setdefault('tumor_stage', tmp[2])
        meta_dict.setdefault('clinical_stage', tmp[3])
    if property == 'exposure':

        sql = 'SELECT JSON_EXTRACT(patient_info,\'$**."clin_shared:number_pack_years_smoked".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."shared:tobacco_smoking_history".text\'),' \
              'JSON_EXTRACT(patient_info,\'$**."clin_shared:year_of_tobacco_smoking_onset".text\') from patient where tcga_case_id=\''+tcga_case_id+'\''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

        meta_dict.setdefault('number_pack_years_smoked', tmp[0])
        meta_dict.setdefault('tobacco_smoking_history', tmp[1])
        meta_dict.setdefault('year_of_tobacco_smoking_onset', tmp[2])

    if property == 'family_history':
        sql = ''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

    if property == 'molecular_test':
        sql = ''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

    if property == 'treatment':
        sql = ''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

    if property == 'follow_up':
        sql = ''

        mycursor = db.cursor(buffered=True)
        mycursor.execute(sql)
        tmp = mycursor.fetchone()
        mycursor.close()

    # metaJson=json.dumps(metaDict)
    pattern = re.compile(r'\[".*"\]')
    for property,value in meta_dict.items():
        if pattern.search(str(value)):
        #     new_value = re.search(r'(\[\")(.*)(\"\])', value).group(2)
        #     if re.search(r'.*", ".*',new_value):
        #         new_value=re.search(r'(.*)(", ")(.*)',new_value).group(1)
        #     meta_dict[property]=new_value
            new_value=re.search('(\[\")(.*)(\")',value.split(',')[0]).group(2)
            meta_dict[property] = new_value

    return meta_dict


def get_bio_data(property, slide_id, db,dia_slide=False):

    sql1 = "SELECT tcga_case_id,tcga_wsi_id from wsi WHERE slide_id="+slide_id
    mycursor = db.cursor()
    mycursor.execute(sql1)
    slide_info=mycursor.fetchone()
    tcga_case_id = str(slide_info[0])
    tcga_wsi_id=str(slide_info[1])
    mycursor.close()

    sql2 = "SELECT bio_id from biospecimen WHERE tcga_case_id=\'" + tcga_case_id+'\''
    mycursor = db.cursor()
    mycursor.execute(sql2)
    slide_info = mycursor.fetchone()
    bio_id = str(slide_info[0])
    mycursor.close()

    meta_dict = {}

    if dia_slide is False:

        if property == 'slide':

            if re.search('.*TS.*',bio_id):
                meta_dict.setdefault('section_location', 'TOP')
            elif re.search('.*TS.*', bio_id):
                meta_dict.setdefault('section_location', 'BOTTOM')

            sql3='SELECT JSON_EXTRACT(bio_info,\'$**."shared:bcr_slide_barcode".text\') from biospecimen where bio_id=\''+bio_id+'\''

            mycursor = db.cursor(buffered=True)
            mycursor.execute(sql3)
            tmp = mycursor.fetchone()[0]
            mycursor.close()

            tmp = re.sub('\[\"', '', tmp)
            tmp = re.sub('\"\]', '', tmp)
            bio_list = re.split(r'", "', tmp)
            print(slide_id)
            print(bio_list)
            print(tcga_wsi_id)
            slide_order=bio_list.index(tcga_wsi_id)

            sql4= 'SELECT JSON_EXTRACT(bio_info,\'$**."bio:percent_eosinophil_infiltration"\'),'\
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_granulocyte_infiltration"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_inflam_infiltration"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_lymphocyte_infiltration"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_monocyte_infiltration"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_necrosis"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_neutrophil_infiltration"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_normal_cells"\'),'\
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_stromal_cells"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_tumor_nuclei"\'),' \
                       'JSON_EXTRACT(bio_info,\'$**."bio:percent_tumor_cells"\') from biospecimen where bio_id=\''+bio_id+'\''

            mycursor = db.cursor(buffered=True)
            mycursor.execute(sql4)
            tmp = mycursor.fetchone()
            mycursor.close()

            tmp0= re.split(r'}, {', tmp[0])
            if re.search('text',tmp0[slide_order]) is None:
                meta_dict.setdefault('percent_eosinophil_infiltration', 'null')
            else:
                tmp0 = re.split(',', tmp0[slide_order])
                for i in tmp0:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_eosinophil_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp1 = re.split(r'}, {', tmp[1])
            if re.search('text', tmp1[slide_order]) is None:
                meta_dict.setdefault('percent_granulocyte_infiltration', 'null')
            else:
                tmp1 = re.split(',', tmp1[slide_order])
                for i in tmp1:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_granulocyte_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp2 = re.split(r'}, {', tmp[2])
            if re.search('text', tmp2[slide_order]) is None:
                meta_dict.setdefault('percent_inflam_infiltration', 'null')
            else:
                tmp2 = re.split(',', tmp2[slide_order])
                for i in tmp2:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_inflam_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp3 = re.split(r'}, {', tmp[3])
            if re.search('text', tmp3[slide_order]) is None:
                meta_dict.setdefault('percent_lymphocyte_infiltration', 'null')
            else:
                tmp3 = re.split(',', tmp3[slide_order])
                for i in tmp3:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_lymphocyte_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp4 = re.split(r'}, {', tmp[4])
            if re.search('text', tmp4[slide_order]) is None:
                meta_dict.setdefault('percent_monocyte_infiltration', 'null')
            else:
                tmp4 = re.split(',', tmp4[slide_order])
                for i in tmp4:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_monocyte_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp5 = re.split(r'}, {', tmp[5])
            if re.search('text', tmp5[slide_order]) is None:
                meta_dict.setdefault('percent_necrosis', 'null')
            else:
                tmp5 = re.split(',', tmp5[slide_order])
                for i in tmp5:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_necrosis', re.search('( "text": ")(.*)(")', i).group(2))

            tmp6 = re.split(r'}, {', tmp[6])
            if re.search('text', tmp6[slide_order]) is None:
                meta_dict.setdefault('percent_neutrophil_infiltration', 'null')
            else:
                tmp6 = re.split(',', tmp6[slide_order])
                for i in tmp6:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_neutrophil_infiltration', re.search('( "text": ")(.*)(")', i).group(2))

            tmp7 = re.split(r'}, {', tmp[7])
            if re.search('text', tmp7[slide_order]) is None:
                meta_dict.setdefault('percent_normal_cells', 'null')
            else:
                tmp7 = re.split(',', tmp7[slide_order])
                for i in tmp7:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_normal_cells', re.search('( "text": ")(.*)(")', i).group(2))

            tmp8 = re.split(r'}, {', tmp[8])
            if re.search('text', tmp8[slide_order]) is None:
                meta_dict.setdefault('percent_stromal_cells', 'null')
            else:
                tmp8 = re.split(',', tmp8[slide_order])
                for i in tmp8:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_stromal_cells', re.search('( "text": ")(.*)(")', i).group(2))

            tmp9 = re.split(r'}, {', tmp[9])
            if re.search('text', tmp9[slide_order]) is None:
                meta_dict.setdefault('percent_tumor_cells', 'null')
            else:
                tmp9 = re.split(',', tmp9[slide_order])
                for i in tmp9:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_tumor_cells', re.search('( "text": ")(.*)(")', i).group(2))

            tmp10 = re.split(r'}, {', tmp[10])
            if re.search('text', tmp10[slide_order]) is None:
                meta_dict.setdefault('percent_tumor_nuclei', 'null')
            else:
                tmp10 = re.split(',', tmp10[slide_order])
                for i in tmp10:
                    if re.search('text', i):
                        meta_dict.setdefault('percent_tumor_nuclei', re.search('( "text": ")(.*)(")', i).group(2))

        if property == 'sample':

            sql3='SELECT JSON_EXTRACT(bio_info,\'$**."bio:bcr_sample_barcode".text\') from biospecimen where bio_id=\''+bio_id+'\''

            mycursor = db.cursor(buffered=True)
            mycursor.execute(sql3)
            tmp = mycursor.fetchone()[0]
            mycursor.close()

            tmp = re.sub('\[\"', '', tmp)
            tmp = re.sub('\"\]', '', tmp)
            bio_list = re.split(r'", "', tmp)
            sample_order = bio_list.index(re.search(r'(TCGA-\w{2}-\w{4}-\w{3})(-\w{2}-\w{3})',tcga_wsi_id).group(1))

        # if type == 'read_group':
        #
        #     sql = ''
        #
        #     mycursor = db.cursor(buffered=True)
        #     mycursor.execute(sql)
        #     tmp = mycursor.fetchone()
        #     mycursor.close()

        if property == 'portion':

            sql3 = 'SELECT JSON_EXTRACT(bio_info,\'$**."bio:bcr_portion_barcode".text\') from biospecimen where bio_id=\'' + bio_id + '\''

            mycursor = db.cursor(buffered=True)
            mycursor.execute(sql3)
            tmp = mycursor.fetchone()[0]
            mycursor.close()

            tmp = re.sub('\[\"', '', tmp)
            tmp = re.sub('\"\]', '', tmp)
            bio_list = re.split(r'", "', tmp)
            portion_order = bio_list.index(re.search(r'(TCGA-\w{2}-\w{4}-\w{3}-\w{2})(-\w{3})',tcga_wsi_id).group(1))

            sql4 = 'SELECT JSON_EXTRACT(bio_info,\'$**."bio:is_ffpe"\'),' \
                   'JSON_EXTRACT(bio_info,\'$**."bio:portion_number"\'),' \
                   'JSON_EXTRACT(bio_info,\'$**."bio:weight"\') from biospecimen where bio_id=\'' + bio_id + '\''

            mycursor = db.cursor(buffered=True)
            mycursor.execute(sql4)
            tmp = mycursor.fetchone()
            mycursor.close()

            tmp0 = re.split(r'}, {', tmp[0])
            if re.search('text', tmp0[portion_order]) is None:
                meta_dict.setdefault('is_ffpe', 'null')
            else:
                tmp0 = re.split(',', tmp0[portion_order])
                for i in tmp0:
                    if re.search('text', i):
                        meta_dict.setdefault('is_ffpe',
                                             re.search('( "text": ")(.*)(")', i).group(2))

            tmp1 = re.split(r'}, {', tmp[1])
            if re.search('text', tmp1[portion_order]) is None:
                meta_dict.setdefault('portion_number', 'null')
            else:
                tmp1 = re.split(',', tmp1[portion_order])
                for i in tmp1:
                    if re.search('text', i):
                        meta_dict.setdefault('portion_number',
                                             re.search('( "text": ")(.*)(")', i).group(2))

            tmp2 = re.split(r'}, {', tmp[2])
            if re.search('text', tmp2[portion_order]) is None:
                meta_dict.setdefault('weight', 'null')
            else:
                tmp2 = re.split(',', tmp2[portion_order])
                for i in tmp2:
                    if re.search('text', i):
                        meta_dict.setdefault('weight', re.search('( "text": ")(.*)(")', i).group(2))

        # if type == 'analyte':
        #     sql = ''
        #
        #     mycursor = db.cursor(buffered=True)
        #     mycursor.execute(sql)
        #     tmp = mycursor.fetchone()
        #     mycursor.close()
        #
        # if type == 'aliquot':
        #     sql = ''
        #
        #     mycursor = db.cursor(buffered=True)
        #     mycursor.execute(sql)
        #     tmp = mycursor.fetchone()
        #     mycursor.close()

        # metaJson=json.dumps(metaDict)
        pattern = re.compile(r'\[".*"\]')
        for attribute, value in meta_dict.items():
            if pattern.search(str(value)):
                new_value = re.search(r'(\[\")(.*)(\"\])', value).group(2)
                if re.search(r'.*", ".*', new_value):
                    new_value = re.search(r'(.*)(", ")(.*)', new_value).group(1)
                meta_dict[attribute] = new_value

    elif dia_slide is True:
        sql3_1='SELECT uuid FROM wsi WHERE slide_id='+slide_id
        mycursor = db.cursor()
        mycursor.execute(sql3_1)
        uuid = mycursor.fetchone()[0]
        mycursor.close()

        sql3_2='SELECT JSON_EXTRACT(bio_info,\'$**."bio:diagnostic_slides"\') from biospecimen where bio_id=\'' + bio_id + '\''
        mycursor = db.cursor()
        mycursor.execute(sql3_2)
        diagnostic_slides = mycursor.fetchone()[0]
        mycursor.close()

        # print(re.search('(\[")(.*)("\])',ffpe_slide_uuid).group(2))
        diagnostic_slides=re.split(r'}, {', diagnostic_slides)
        for slide in diagnostic_slides:
            if re.search('text', slide):
                sample_order=diagnostic_slides.index(slide)

    # select sample information based on sample_order

    sql4 = 'SELECT JSON_EXTRACT(bio_info,\'$**."bio:sample_type"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:tissue_type"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:composition"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:current_weight"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:days_to_collection"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:freezing_method"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:initial_weight"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:intermediate_dimension"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:is_ffpe"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:longest_dimension"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:method_of_sample_procurement"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:oct_embedded"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:pathology_report_uuid"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:preservation_method"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:sample_type_id"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:shortest_dimension"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:time_between_clamping_and_freezing"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:time_between_excision_and_freezing"\'),' \
           'JSON_EXTRACT(bio_info,\'$**."bio:tumor_descriptor"\') from biospecimen where bio_id=\'' + bio_id + '\''

    mycursor = db.cursor(buffered=True)
    mycursor.execute(sql4)
    tmp = mycursor.fetchone()
    mycursor.close()

    tmp0 = re.split(r'}, {', tmp[0])
    if re.search('text', tmp0[sample_order]) is None:
        meta_dict.setdefault('sample_type', 'null')
    else:
        tmp0 = re.split(',', tmp0[sample_order])
        for i in tmp0:
            if re.search('text', i):
                meta_dict.setdefault('sample_type', re.search('( "text": ")(.*)(")', i).group(2))

    tmp1 = re.split(r'}, {', tmp[1])
    if re.search('text', tmp1[sample_order]) is None:
        meta_dict.setdefault('tissue_type', 'null')
    else:
        tmp1 = re.split(',', tmp1[sample_order])
        for i in tmp1:
            if re.search('text', i):
                meta_dict.setdefault('tissue_type', re.search('( "text": ")(.*)(")', i).group(2))

    tmp2 = re.split(r'}, {', tmp[2])
    if re.search('text', tmp2[sample_order]) is None:
        meta_dict.setdefault('composition', 'null')
    else:
        tmp2 = re.split(',', tmp2[sample_order])
        for i in tmp2:
            if re.search('text', i):
                meta_dict.setdefault('composition', re.search('( "text": ")(.*)(")', i).group(2))

    tmp3 = re.split(r'}, {', tmp[3])
    if re.search('text', tmp3[sample_order]) is None:
        meta_dict.setdefault('current_weight', 'null')
    else:
        tmp3 = re.split(',', tmp3[sample_order])
        for i in tmp3:
            if re.search('text', i):
                meta_dict.setdefault('current_weight', re.search('( "text": ")(.*)(")', i).group(2))

    tmp4 = re.split(r'}, {', tmp[4])
    if re.search('text', tmp4[sample_order]) is None:
        meta_dict.setdefault('days_to_collection', 'null')
    else:
        tmp4 = re.split(',', tmp4[sample_order])
        for i in tmp4:
            if re.search('text', i):
                meta_dict.setdefault('days_to_collection', re.search('( "text": ")(.*)(")', i).group(2))

    tmp5 = re.split(r'}, {', tmp[5])
    if re.search('text', tmp5[sample_order]) is None:
        meta_dict.setdefault('freezing_method', 'null')
    else:
        tmp5 = re.split(',', tmp5[sample_order])
        for i in tmp5:
            if re.search('text', i):
                meta_dict.setdefault('freezing_method', re.search('( "text": ")(.*)(")', i).group(2))

    tmp6 = re.split(r'}, {', tmp[6])
    if re.search('text', tmp6[sample_order]) is None:
        meta_dict.setdefault('initial_weight', 'null')
    else:
        tmp6 = re.split(',', tmp6[sample_order])
        for i in tmp6:
            if re.search('text', i):
                meta_dict.setdefault('initial_weight', re.search('( "text": ")(.*)(")', i).group(2))

    tmp7 = re.split(r'}, {', tmp[7])
    if re.search('text', tmp7[sample_order]) is None:
        meta_dict.setdefault('intermediate_dimension', 'null')
    else:
        tmp7 = re.split(',', tmp7[sample_order])
        for i in tmp7:
            if re.search('text', i):
                meta_dict.setdefault('intermediate_dimension', re.search('( "text": ")(.*)(")', i).group(2))

    tmp8 = re.split(r'}, {', tmp[8])
    if re.search('text', tmp8[sample_order]) is None:
        meta_dict.setdefault('is_ffpe', 'null')
    else:
        tmp8 = re.split(',', tmp8[sample_order])
        for i in tmp8:
            if re.search('text', i):
                meta_dict.setdefault('is_ffpe', re.search('( "text": ")(.*)(")', i).group(2))

    tmp9 = re.split(r'}, {', tmp[9])
    if re.search('text', tmp9[sample_order]) is None:
        meta_dict.setdefault('longest_dimension', 'null')
    else:
        tmp9 = re.split(',', tmp9[sample_order])
        for i in tmp9:
            if re.search('text', i):
                meta_dict.setdefault('longest_dimension', re.search('( "text": ")(.*)(")', i).group(2))

    tmp10 = re.split(r'}, {', tmp[10])
    if re.search('text', tmp10[sample_order]) is None:
        meta_dict.setdefault('method_of_sample_procurement', 'null')
    else:
        tmp10 = re.split(',', tmp10[sample_order])
        for i in tmp10:
            if re.search('text', i):
                meta_dict.setdefault('method_of_sample_procurement', re.search('( "text": ")(.*)(")', i).group(2))

    tmp11 = re.split(r'}, {', tmp[11])
    if re.search('text', tmp11[sample_order]) is None:
        meta_dict.setdefault('oct_embedded', 'null')
    else:
        tmp11 = re.split(',', tmp11[sample_order])
        for i in tmp11:
            if re.search('text', i):
                meta_dict.setdefault('oct_embedded', re.search('( "text": ")(.*)(")', i).group(2))

    tmp12 = re.split(r'}, {', tmp[12])
    if re.search('text', tmp12[sample_order]) is None:
        meta_dict.setdefault('pathology_report_uuid', 'null')
    else:
        tmp12 = re.split(',', tmp12[sample_order])
        for i in tmp12:
            if re.search('text', i):
                meta_dict.setdefault('pathology_report_uuid', re.search('( "text": ")(.*)(")', i).group(2))

    tmp13 = re.split(r'}, {', tmp[13])
    if re.search('text', tmp13[sample_order]) is None:
        meta_dict.setdefault('preservation_method', 'null')
    else:
        tmp13 = re.split(',', tmp13[sample_order])
        for i in tmp13:
            if re.search('text', i):
                meta_dict.setdefault('preservation_method', re.search('( "text": ")(.*)(")', i).group(2))

    tmp14 = re.split(r'}, {', tmp[14])
    if re.search('text', tmp14[sample_order]) is None:
        meta_dict.setdefault('sample_type_id', 'null')
    else:
        tmp14 = re.split(',', tmp14[sample_order])
        for i in tmp14:
            if re.search('text', i):
                meta_dict.setdefault('sample_type_id', re.search('( "text": ")(.*)(")', i).group(2))

    tmp15 = re.split(r'}, {', tmp[15])
    if re.search('text', tmp15[sample_order]) is None:
        meta_dict.setdefault('shortest_dimension', 'null')
    else:
        tmp15 = re.split(',', tmp15[sample_order])
        for i in tmp15:
            if re.search('text', i):
                meta_dict.setdefault('shortest_dimension', re.search('( "text": ")(.*)(")', i).group(2))

    tmp16 = re.split(r'}, {', tmp[16])
    if re.search('text', tmp16[sample_order]) is None:
        meta_dict.setdefault('time_between_clamping_and_freezing', 'null')
    else:
        tmp16 = re.split(',', tmp16[sample_order])
        for i in tmp16:
            if re.search('text', i):
                meta_dict.setdefault('time_between_clamping_and_freezing', re.search('( "text": ")(.*)(")', i).group(2))

    tmp17 = re.split(r'}, {', tmp[17])
    if re.search('text', tmp17[sample_order]) is None:
        meta_dict.setdefault('time_between_excision_and_freezing', 'null')
    else:
        tmp17 = re.split(',', tmp17[sample_order])
        for i in tmp17:
            if re.search('text', i):
                meta_dict.setdefault('time_between_excision_and_freezing', re.search('( "text": ")(.*)(")', i).group(2))

    tmp18 = re.split(r'}, {', tmp[18])
    if re.search('text', tmp18[sample_order]) is None:
        meta_dict.setdefault('tumor_descriptor', 'null')
    else:
        tmp18 = re.split(',', tmp18[sample_order])
        for i in tmp18:
            if re.search('text', i):
                meta_dict.setdefault('tumor_descriptor', re.search('( "text": ")(.*)(")', i).group(2))

    return meta_dict


def get_tba_list(db, slide_id):
    """
    This function will fetch the to be annotated list from the MySQL database based on the give slide id.
    :param db: OpenHI database object.
    :param slide_id: (int) slide id
    :return: (list) tuple of x and y coordinate.
    """

    sql_cmd = 'SELECT sw_id, center_x, center_y FROM tba_list WHERE slide_id = ' + str(slide_id)
    # print(sql_cmd)

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    myresult = mycursor.fetchall()
    mycursor.close()

    return myresult


def get_center_tb_reg(db, slide_id, reg_id):
    sql_cmd = 'SELECT center_x, center_y FROM tba_list WHERE slide_id = ' \
              + str(slide_id) + ' AND sw_id = ' + str(reg_id)

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    myresult = mycursor.fetchall()
    mycursor.close()

    return myresult[0][0], myresult[0][1]


def add_to_tba_list(db, slide_id, coordinate):
    sql_cmd = 'INSERT INTO tba_list (`slide_id`, `center_x`, `center_y`) VALUES (' \
              + str(slide_id) + ', ' \
              + str(coordinate[0]) + ', ' + str(coordinate[1]) + ')'

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    mycursor.close()


def rm_from_tba_list(db, slide_id, sw_id):
    sql_cmd = 'DELETE FROM tba_list WHERE `sw_id` = ' + str(sw_id) + ' AND `slide_id` = ' + str(slide_id)

    mycursor = db.cursor()
    mycursor.execute(sql_cmd)
    mycursor.close()


if __name__ == '__main__':
    print('This function is not made to be the main function!')
