from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField,BooleanField
from wtforms.validators import DataRequired


# class RegistrationForm(FlaskForm):
#     username=StringField('Username',
#                          validators=[DataRequired(),Length(min=2,max=20)])
#     email=StringField('Email',
#                       validators=[DataRequired(),Email()])
#     password=PasswordField('password',
#                            validators=[DataRequired()])
#     confirm_password=PasswordField('Confirm Password',
#                                    validators=[DataRequired(),EqualTo('password')])
#     submit=SubmitField('sign up')


class LoginForm(FlaskForm):
    username = StringField('Username',
                        validators=[DataRequired()])
    password = PasswordField('password',
                             validators=[DataRequired()])
    remember=BooleanField('Remember Me')
    submit=SubmitField('login')
